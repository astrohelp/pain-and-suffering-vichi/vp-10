﻿module VP04
implicit none

    contains
    
    subroutine gauss(Extended,X,n)
    real(8), dimension(n+1,n) :: Extended
    real(8), dimension(n) :: X, Temp
    integer(4) :: i, j, k, n
    real(8), parameter :: eps=2e-4
    logical, parameter :: IamJordan=.FALSE.

    do k=1,n-1
        if (abs(Extended(k,k)) < eps) write(*,*) 'Warning: элемент близок к нулю'
        
        forall (j=k:n+1) Extended(j,k)=Extended(j,k)/Extended(k,k)
        forall (i=k+1:n, j=k:n+1) Extended(j,i)=Extended(j,i)-Extended(k,i)*Extended(j,k)
    enddo
    Extended(n:n+1,n)=Extended(n:n+1,n)/Extended(n,n)
    
    call MatrixOperations(n,X,Extended,IamJordan)
    end subroutine gauss
    
    subroutine jordan(Extended,X,n)
    real(8), dimension(n+1,n) :: Extended
    real(8), dimension(n) :: X
    integer(4) :: i, j, k, n
    logical, parameter :: IamJordan=.TRUE.
    
    do k=1,n
        forall (j=k:n+1) Extended(j,k)=Extended(j,k)/Extended(k,k)
        forall (i=1:k-1, j=k:n+1) Extended(j,i)=Extended(j,i)-Extended(k,i)*Extended(j,k)
        forall (i=k+1:n, j=k:n+1) Extended(j,i)=Extended(j,i)-Extended(k,i)*Extended(j,k)
    enddo
    Extended(n+1,n)=Extended(n+1,n)/Extended(n,n)

    call MatrixOperations(n,X,Extended,IamJordan)
    end subroutine jordan
    
    subroutine choise(Extended,X,n)
    real(8), dimension(n+1,n) :: Extended
    real(8), dimension(n) :: X
    integer(4) :: i, j, k, n, coord
    real(8), dimension(n+1) :: NewX, Temp
    integer(4), dimension(2,n) :: CoordinatesOfChanged
    logical, parameter :: IamJordan=.FALSE.
    
    do k=1,n-1
        if ( abs( maxval(Extended(k:n,k:n)) ) > abs( minval(Extended(k:n,k:n)) ) ) then 
	        CoordinatesOfChanged(1:2,k)=(k-1)+maxloc(Extended(k:n,k:n))
        else
	        CoordinatesOfChanged(1:2,k)=(k-1)+minloc(Extended(k:n,k:n))
        endif
        Coord=CoordinatesOfChanged(1,k)
        newX(1:n)=Extended(Coord,1:n)
        Extended(Coord,1:n)=Extended(k,1:n)
        Extended(k,1:n)=newX(1:n)
        
        Coord=CoordinatesOfChanged(2,k)
        NewX(1:n+1)=Extended(1:n+1,Coord)
        Extended(1:n+1,Coord)=Extended(1:n+1,k)
        Extended(1:n+1,k)=newX(1:n+1)
        
        forall (j=k:n+1) Extended(j,k)=Extended(j,k)/Extended(k,k)
        forall (i=k+1:n, j=k:n+1) Extended(j,i)=Extended(j,i)-Extended(k,i)*Extended(j,k)
    enddo
    
    Extended(n:n+1,n)=Extended(n:n+1,n)/Extended(n,n)
    
    call MatrixOperations(n,X,Extended,IamJordan)
    
    do k=n-1,1,-1
        Coord=CoordinatesOfChanged(1,k)
        NewX(k)=X(Coord)
        X(Coord)=X(k)
        X(k)=NewX(k)
    enddo
    end subroutine choise
    
    subroutine MatrixOperations(n,X,Extended,IamJordan)
    integer(4) :: i, n
    real(8), dimension(n+1,n) :: Extended
    real(8), dimension(n) :: X
    logical :: IamJordan
    
    if (IamJordan) then
        do i=1,n
	        X(i)=Extended(n+1,i)
        enddo
    else
        do i=n,1,-1
	        X(i)=Extended(n+1,i)-dot_product(Extended(i+1:n,i),X(i+1:n))
        enddo
    endif   
        
    end subroutine MatrixOperations
    
    function loss(A,X,B,n)
    integer(4) :: n
    real(8), dimension(n) :: X, B
    real(8), dimension(n,n) :: A
    real(8) :: loss
        loss = sqrt(sum((matmul(transpose(A),X)-B)**2))
    end function loss
        
    subroutine output(n,X,mettype,A,B)
    character(6) :: mettype
    integer(4) :: n, i
    real(8), dimension(n) :: X, B
    real(8), dimension(n,n) :: A
    if (mettype=='all') then
        open(2,file='result.dat', position = 'append', status='old')       
    else    
        open(2,file='result.dat',status='replace')
    endif    
        write(2,'("# ",i8)') n
        do i=1,n
            write(2,*) X(i)
        enddo
        close(2)
        write(*,*) "модуль вектора невязки ",  loss(A,X,B,n)
    end subroutine output
end module VP04   
