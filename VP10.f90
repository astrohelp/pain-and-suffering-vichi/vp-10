﻿program VP10

    use params
    use basov
    implicit none
    
    real(8), dimension(0:int(interval_end/h) + 1, 1:ndim) :: X
    character(6) :: method

    call getarg(1, method)

    select case(method)
        case('rosen ')
                call Rosen(X)
        case('precor')
                call Precor(X)
        case default
            stop 'Пожалуйста, выберете метод: rosen для метода &
                Розенброка, precor для &
                Предикторно-корректорный метода.'
    end select
   
end program VP10
